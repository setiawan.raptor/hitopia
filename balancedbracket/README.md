# Balanced Bracket soal nomor 1

## Deskripsi
This program determines whether a string containing the brackets `(`, `)`, `{`, `}`, `[`, `]` has balanced brackets. A parenthesis is balanced if each open parenthesis has a corresponding close pair in the correct order.

## Contoh
### Sampel 1
- **Input:** `([{}])`
- **Output:** `YES`

### Sampel 2
- **Input:** `([{]})`
- **Output:** `NO`

### Sampel 3
- **Input:** `({[]})`
- **Output:** `YES`

## Role
1. Allowed brackets are: `(`, `)`, `{`, `}`, `[`, `]`.
2. Brackets can be separated with or without whitespace.
3. Check for matching brackets between the opening and closing brackets by returning the string value `YES` or `NO`.

## Complexity
### Time Complexity
where n is the length of the string. Each character in the string is checked once.

### Space Complexity
all characters in a string can be put on the stack if they are all open brackets.

## How to use
Run the program and enter the bracketed string when prompted. The program will print `YES` if the string is balanced, and `NO` if not.

## Usage Example
```bash
$ java BalancedBracket
Input: ([{}])
Output: YES

$ java BalancedBracket
Input: ([{]})
Output: NO
 