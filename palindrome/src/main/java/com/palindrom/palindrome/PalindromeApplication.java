package com.palindrom.palindrome;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class PalindromeApplication {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number string  s: ");
		String s = scanner.next();
		System.out.println("Enter the maximum number of changes (k): ");
		int k = scanner.nextInt();

		String result = highestPalindrome(s, k, 0, s.length() - 1);
		System.out.println(result.equals("") ? "-1" : result);
	}

	private static String highestPalindrome(String s, int k, int left, int right) {
		if (left > right) {
			return s;
		}

		char[] arr = s.toCharArray();

		// Base case: If we only have one digit left and we still have changes left
		if (left == right) {
			if (k > 0) {
				arr[left] = '9';
			}
			return new String(arr);
		}

		if (arr[left] != arr[right]) {
			if (k == 0) {
				return "";
			}
			if (arr[left] > arr[right]) {
				arr[right] = arr[left];
			} else {
				arr[left] = arr[right];
			}
			s = new String(arr);
			return highestPalindrome(s, k - 1, left + 1, right - 1);
		} else {
			String result = highestPalindrome(s, k, left + 1, right - 1);
			if (!result.equals("")) {
				return result;
			}
			if (k > 0) {
				if (arr[left] != '9') {
					arr[left] = arr[right] = '9';
					s = new String(arr);
					return highestPalindrome(s, k - 1, left + 1, right - 1);
				}
			}
			return "";
		}
	}
}
